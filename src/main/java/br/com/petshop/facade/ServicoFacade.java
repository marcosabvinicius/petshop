package br.com.petshop.facade;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.petshop.dao.ServicoDao;
import br.com.petshop.model.Servico;

public class ServicoFacade extends Facade implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject 
	private ServicoDao dao;
	
	public void save(Servico servico) {
		dao.save(servico);
	}
	
	public void update(Servico servico) {
		dao.update(servico);
	}
	
    public List<Servico> selectAll() {
        return this.dao.selectAll();
    }
	
    public void delete(Servico servico) {
    	dao.delete(servico);
    }
    
	public Servico selectById(Integer servicoId) {
        return this.dao.selectById(servicoId);
    }

	@Override
	public void save() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void select() {
		// TODO Auto-generated method stub
		
	}
}
