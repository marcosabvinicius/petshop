package br.com.petshop.facade;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.petshop.dao.CachorroDao;
import br.com.petshop.model.Cachorro;

public class CachorroFacade extends Facade implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject 
	private CachorroDao dao;
	
	public void save(Cachorro cachorro) {
		dao.save(cachorro);
	}
	
	public void update(Cachorro cachorro) {
		dao.update(cachorro);
	}
	
    public List<Cachorro> selectAll() {
        return this.dao.selectAll();
    }
	
    public void delete(Cachorro cachorro) {
    	dao.delete(cachorro);
    }
    
	public Cachorro selectById(Integer cachorroId) {
        return this.dao.selectById(cachorroId);
    }

	@Override
	public void save() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void select() {
		// TODO Auto-generated method stub
		
	}

}
