package br.com.petshop.facade;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.petshop.dao.ClienteDao;
import br.com.petshop.model.Cliente;

public class ClienteFacade extends Facade implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject 
	private ClienteDao dao;
	
	public void save(Cliente cliente) {
		dao.save(cliente);
	}
	
	public void update(Cliente cliente) {
		dao.update(cliente);
	}
	
    public List<Cliente> selectAll() {
        return this.dao.selectAll();
    }
	
    public void delete(Cliente cliente) {
    	dao.delete(cliente);
    }
    
	public Cliente selectById(Integer clienteId) {
        return this.dao.selectById(clienteId);
    }

	@Override
	public void save() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void select() {
		// TODO Auto-generated method stub
		
	}

	
}
