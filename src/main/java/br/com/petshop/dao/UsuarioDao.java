package br.com.petshop.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.petshop.model.Cliente;
import br.com.petshop.model.Usuario;

public class UsuarioDao implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
    EntityManager em;
	
	private DAO<Usuario> dao;

	@PostConstruct //o CDI vai chamar esse m�todo assim que inicializar CachorroDao
    void init() {
        this.dao = new DAO<Usuario>(em, Usuario.class);
    }
	
	public Usuario selectById(Integer usuarioId) {
        return this.dao.selectById(usuarioId);
    }

    public void save(Usuario usuario) {
    	this.dao.save(usuario);
    }

    public void update(Usuario usuario) {
    	this.dao.update(usuario);
    }

    public void delete(Usuario usuario) {
    	this.dao.delete(usuario);
    }

    public List<Usuario> selectAll() {
        return this.dao.selectAll();
    }
	
	public boolean exists(Usuario usuario) {
        
		EntityManager em = new JPAUtil().getEntityManager();
        
		TypedQuery<Usuario> query = em.createQuery("select u from Usuario u where u.email = :pEmail and u.senha = :pSenha", Usuario.class);
        
		query.setParameter("pEmail", usuario.getEmail());
	    query.setParameter("pSenha", usuario.getSenha());

	    try {
	        Usuario resultado = query.getSingleResult();
	    } catch (NoResultException ex) {
	        return false;
	    }

	    em.close();

	    return true;
    }
	
}
