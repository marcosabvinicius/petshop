package br.com.petshop.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.petshop.model.Cachorro;

public class CachorroDao implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
    EntityManager entityManager;
	
	private DAO<Cachorro> dao;

	@PostConstruct //o CDI vai chamar esse m�todo assim que inicializar CachorroDao
    void init() {
        this.dao = new DAO<Cachorro>(entityManager, Cachorro.class);
    }
	
	public Cachorro selectById(Integer cachorroId) {
        return this.dao.selectById(cachorroId);
    }

    public void save(Cachorro cachorro) {
    	this.dao.save(cachorro);
    }

    public void update(Cachorro cachorro) {
    	this.dao.update(cachorro);
    }

    public void delete(Cachorro cachorro) {
    	this.dao.delete(cachorro);
    }

    public List<Cachorro> selectAll() {
        return this.dao.selectAll();
    }
}
