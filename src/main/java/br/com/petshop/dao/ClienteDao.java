package br.com.petshop.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.petshop.model.Cliente;


public class ClienteDao implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
    EntityManager em;
	
	private DAO<Cliente> dao;

	@PostConstruct //o CDI vai chamar esse m�todo assim que inicializar CachorroDao
    void init() {
        this.dao = new DAO<Cliente>(em, Cliente.class);
    }
	
	public Cliente selectById(Integer clienteId) {
        return this.dao.selectById(clienteId);
    }

    public void save(Cliente cliente) {
    	this.dao.save(cliente);
    }

    public void update(Cliente cliente) {
    	this.dao.update(cliente);
    }

    public void delete(Cliente cliente) {
    	this.dao.delete(cliente);
    }

    public List<Cliente> selectAll() {
        return this.dao.selectAll();
    }
}
