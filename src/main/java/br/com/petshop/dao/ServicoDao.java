package br.com.petshop.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.petshop.model.Servico;


public class ServicoDao implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
    EntityManager em;
	
	private DAO<Servico> dao;

	@PostConstruct //o CDI vai chamar esse m�todo assim que inicializar CachorroDao
    void init() {
        this.dao = new DAO<Servico>(em, Servico.class);
    }
	
	public Servico selectById(Integer servicoId) {
        return this.dao.selectById(servicoId);
    }

    public void save(Servico servico) {
    	this.dao.save(servico);
    }

    public void update(Servico servico) {
    	this.dao.update(servico);
    }

    public void delete(Servico servico) {
    	this.dao.delete(servico);
    }

    public List<Servico> selectAll() {
        return this.dao.selectAll();
    }
}
