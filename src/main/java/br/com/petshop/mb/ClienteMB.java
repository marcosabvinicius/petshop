package br.com.petshop.mb;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.petshop.TX.Transacional;
import br.com.petshop.facade.CachorroFacade;
import br.com.petshop.facade.ClienteFacade;
import br.com.petshop.facade.ServicoFacade;
import br.com.petshop.model.Cachorro;
import br.com.petshop.model.Cliente;
import br.com.petshop.model.Servico;
import br.com.petshop.util.RedirectView;


@Named //anota��o do CDI, antes era a anota��o do JSF
@ViewScoped //Essa anota��o tem o mesmo nome, mas � de outro pacote: javax.faces.view.ViewScoped
public class ClienteMB implements Serializable {

	private static final long serialVersionUID = 1L;

	private Cliente cliente = new Cliente();

	private Integer cachorroId;
	
	private Integer servicoId;

	@Inject
	private ClienteFacade clienteFacade;

	@Inject
	private CachorroFacade cachorroFacade;

	@Inject
	private ServicoFacade servicoFacade;
	
	public ClienteMB() {
	
	}
	
	public Integer getServicoId() {
		return servicoId;
	}

	public void setServicoId(Integer servicoId) {
		this.servicoId = servicoId;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Integer getCachorroId() {
		return cachorroId;
	}

	public void setCachorroId(Integer cachorroId) {
		this.cachorroId = cachorroId;
	}

	public List<Cliente> getClientes() {
		return clienteFacade.selectAll();
	}
	
	public List<Cachorro> getCachorros() {
		return cachorroFacade.selectAll();	
	}

	public List<Servico> getServicos() {
		return servicoFacade.selectAll();
	}
	
	public List<Cachorro> getCachorrosDoCliente() {
		return this.cliente.getCachorros();
	}

	public List<Servico> getServicosDoCliente() {
		return this.cliente.getServicos();
	}
	
	public void gravarCachorro() {
		Cachorro cachorro = cachorroFacade.selectById(this.cachorroId);
		this.cliente.adicionaCachorro(cachorro);
	}

	public void gravarServico() {
		Servico servico = servicoFacade.selectById(this.servicoId);
		this.cliente.adicionaServico(servico);
	}
	
	
	@Transacional
	public void gravar() {
		System.out.println("Gravando cliente " + this.cliente.getNome());
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		validaCachorroPreenchido();
		/*
		if (cliente.getCachorros().isEmpty()) {
			
			context.getExternalContext().getFlash().setKeepMessages(true);
	        context.addMessage(null, new FacesMessage("Cliente deve ter pelo menos um Cachorro .."));
	        
	        throw new RuntimeException("Cliente deve ter pelo menos um Cachorro!");
			
		}
  	*/
       	if (this.cliente.getId() == null) {
			clienteFacade.save(cliente);
		}
		else {
			clienteFacade.save(cliente);
		}
		
		this.cliente = new Cliente();
	}
	
	  public RedirectView validaCachorroPreenchido() {
	        	        
	    	FacesContext context = FacesContext.getCurrentInstance();
	    
	    	if (cliente.getCachorros().isEmpty()) {
				
				context.getExternalContext().getFlash().setKeepMessages(true);
		        context.addMessage(null, new FacesMessage("Cliente deve ter pelo menos um Cachorro .."));
		        
		        throw new RuntimeException("Cliente deve ter pelo menos um Cachorro!");
		        
			}
	    	return new RedirectView("cliente");
	    }
	
	public void carregar(Cliente cliente) {
	    System.out.println("Carregando cliente " + cliente.getNome());
	    this.cliente = cliente;
	}
	
	@Transacional
	public void remover(Cliente cliente) {
	    System.out.println("Removendo cliente " + cliente.getNome());
	    clienteFacade.delete(cliente);
	    
	}
	
	public String formCachorro() {
		System.out.println("Chamando formul�rio de Cachorro");
		return "cachorro";
	}
		
	public String formServico() {
		System.out.println("Chamando formul�rio de Servi�o");
		return "servicos";
	}
	
	public void removerCachorroDoCliente(Cachorro cachorro) {
		this.cliente.removeCachorro(cachorro);
	}
	
	public void removerServicoDoCliente(Servico servico) {
		this.cliente.removeServico(servico);
	}
}
