package br.com.petshop.mb;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.RequestContext;

import br.com.petshop.TX.Transacional;
import br.com.petshop.facade.CachorroFacade;
import br.com.petshop.dao.CachorroDao;
import br.com.petshop.model.Cachorro;
import br.com.petshop.util.RedirectView;

@Named // anota��o do CDI, antes era a anota��o do JSF
@ViewScoped // Essa anota��o tem o mesmo nome, mas � de outro pacote: javax.faces.view.ViewScoped
public class CachorroMB implements Serializable {

	private static final long serialVersionUID = 1L;

	private Cachorro cachorro = new Cachorro();

	private Integer cachorroId;

	private Part fotoCachorro;
	
	@Inject
	private FacesContext context;
	
	private ServletContext request;
	
	@Inject
	private CachorroFacade cachorroControlador;
	
	public CachorroMB() {

	}

	public Integer getCachorroId() {
		return cachorroId;
	}

	public void setCachorroId(Integer cachorroId) {
		this.cachorroId = cachorroId;
	}

	public void setCachorro(Cachorro cachorro) {
		this.cachorro = cachorro;
	}

	public Cachorro getCachorro() {
		return cachorro;
	}
	
	public Part getFotoCachorro() {
		return fotoCachorro;
	}

	public void setFotoCachorro(Part fotoCachorro) {
		this.fotoCachorro = fotoCachorro;
	}

	@Transacional
	public RedirectView gravar() throws IOException {
		System.out.println("Gravando cachorro " + this.cachorro.getNome());
		//String realPath = request.getServletContext().getRealPath("/resources/images");
		if (this.cachorro.getId() == null) {
			criaDiretorio();
			cachorro.setPathImagem("\\resources\\img\\" + fotoCachorro.getSubmittedFileName());
			cachorroControlador.save(this.cachorro);
			fotoCachorro.write(fotoCachorro.getSubmittedFileName());
		} else {
			cachorroControlador.update(this.cachorro);
		}
		this.cachorro = new Cachorro();
		return new RedirectView("cliente");
	}
	
	public List<Cachorro> getCachorros() {
		return cachorroControlador.selectAll();
	}

	@Transacional
	public void delete(Cachorro cachorro) {
		System.out.println("Removendo cachorro " + cachorro.getNome());
		cachorroControlador.delete(cachorro);
	}

	public void select(Cachorro cachorro) {
		System.out.println("Carregando cachorro " + cachorro.getNome());
		this.cachorro = cachorro;
	}

	public void selectById() {
		this.cachorro = cachorroControlador.selectById(cachorroId);
		if (this.cachorro == null) {
			this.cachorro = new Cachorro();
		}
	}
	
	public void criaDiretorio() {
		File diretorio = new File("c:\\fotos");
		if (!diretorio.exists()) {
		   diretorio.mkdirs();
		} else {
		   System.out.println("Diretorio ja existe ..");
		}
	}
	
	public void carregarDetalhe() {
		this.cachorro = cachorroControlador.selectById(cachorroId);
	}

	
}
