package br.com.petshop.mb;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.petshop.TX.Transacional;
import br.com.petshop.facade.UsuarioFacade;
import br.com.petshop.model.Usuario;
import br.com.petshop.util.RedirectView;

@Named
@ViewScoped
public class UsuarioMB implements Serializable{

	private static final long serialVersionUID = 1L;

	private Usuario usuario = new Usuario();

	private Integer usuarioId;

	@Inject
	private UsuarioFacade usuarioFacade;
	
	public UsuarioMB() {
		
	}
		
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	@Transacional
	public RedirectView gravar() {		
		System.out.println("Gravando usuario " + this.usuario.getEmail());		
		if (this.usuario.getId() == null) {
			usuarioFacade.save(this.usuario);
		}
		else {
			usuarioFacade.update(this.usuario);
		}
		this.usuario = new Usuario();		
		return new RedirectView("usuario");
	}
	
	public List<Usuario> getUsuarios() {
		return usuarioFacade.selectAll();
	}
	
	@Transacional
	public void remover(Usuario usuario) {
		System.out.println("Removendo usuario " + usuario.getEmail());
		usuarioFacade.delete(usuario);
	}
	
	public void carregar(Usuario usuario) {
		 System.out.println("Carregando usuario " + usuario.getEmail());
		 this.usuario = usuario;
	}
	
	public void carregarCachorroPeloId() {
		this.usuario = usuarioFacade.selectById(usuarioId);
		if(this.usuario == null) {
			this.usuario = new Usuario();
		}
	} 
}
