package br.com.petshop.mb;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.petshop.TX.Transacional;
import br.com.petshop.facade.ServicoFacade;
import br.com.petshop.model.Servico;
import br.com.petshop.util.RedirectView;

@Named //anota��o do CDI, antes era a anota��o @ManagedBean do JSF
@ViewScoped //Essa anota��o tem o mesmo nome, mas � de outro pacote: javax.faces.view.ViewScoped
public class ServicoMB implements Serializable{
	
private static final long serialVersionUID = 1L;
	
	private Servico servico = new Servico();

	private Integer servicoId;
	
	@Inject
	private ServicoFacade servicoControlador;
	
	public ServicoMB() {
		
	}
	
	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	public Integer getServicoId() {
		return servicoId;
	}

	public void setServicoId(Integer servicoId) {
		this.servicoId = servicoId;
	}

	@Transacional
	public RedirectView gravar() {
		
		System.out.println("Gravando servico " + this.servico.getNome());
		
		if (this.servico.getId() == null) {
			servicoControlador.save(this.servico);
		}
		else {
			servicoControlador.update(this.servico);
		}
		this.servico = new Servico();
		
		return new RedirectView("servicoCliente");
			
	}
	
	public List<Servico> getServicos() {
		return servicoControlador.selectAll();
	}
	
	@Transacional
	public void remover(Servico servico) {
		System.out.println("Removendo servico " + servico.getNome());
		servicoControlador.delete(servico);
	}
	
	public void carregar(Servico servico) {
		 System.out.println("Carregando servico " + servico.getNome());
		 this.servico = servico;
	}
	
	public void carregarServicoPeloId() {
		this.servico = servicoControlador.selectById(servicoId);
		if(this.servico == null) {
			this.servico = new Servico();
		}
	}
}
