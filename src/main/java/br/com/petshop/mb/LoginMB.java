package br.com.petshop.mb;


import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.petshop.facade.UsuarioFacade;
import br.com.petshop.model.Usuario;
import br.com.petshop.util.RedirectView;

@Named //anota��o do CDI, antes era a anota��o @ManagedBean do JSF
@ViewScoped //Essa anota��o tem o mesmo nome, mas � de outro pacote: javax.faces.view.ViewScoped
public class LoginMB implements Serializable{

	private static final long serialVersionUID = 1L;

	private Usuario usuario = new Usuario();

	@Inject
    FacesContext context;
	
	@Inject
	private UsuarioFacade usuarioFacade;
	
    public Usuario getUsuario() {
        return usuario;
    }
    
    public RedirectView efetuaLogin() {
        
    	System.out.println("Fazendo login do usu�rio " + this.usuario.getEmail());
               	
        boolean existe = usuarioFacade.exists(this.usuario);
        
        if(existe) {        
        	context.getExternalContext().getSessionMap().put("usuarioLogado", this.usuario);        	
        	return new RedirectView("cliente");
        }
        
        context.getExternalContext().getFlash().setKeepMessages(true);
        context.addMessage(null, new FacesMessage("Usu�rio n�o encontrado ou senha inv�lida .."));
        
        return new RedirectView("login");
    }
    
    public RedirectView deslogar() {
		
    	FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().remove("usuarioLogado");

        return new RedirectView("login");
    	
    }
    
}
